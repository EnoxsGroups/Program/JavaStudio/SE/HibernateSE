package enoxs.com.data;

import enoxs.com.data.gen.AppInfoEntityGenIntelliJTool;
import enoxs.com.util.HibernateUtil;
import org.hibernate.Session;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Iterator;
import java.util.List;

public class AppInfoEntityTest {
    private static Session session;

    @BeforeClass
    public static void beforeClass(){
        HibernateUtil.initConfig("enoxs.com.data",AppInfoEntityGenIntelliJTool.class);
        HibernateUtil.start();
        session = HibernateUtil.getSession();
    }

    @AfterClass
    public static void afterClass(){
        HibernateUtil.done();
    }

    @Test
    public void query(){
        List appInfos = session.createQuery("FROM enoxs.com.data.AppInfoEntity ").list();
        for (Iterator iterator = appInfos.iterator(); iterator.hasNext(); ) {
            AppInfoEntityGenIntelliJTool appInfoEntity = (AppInfoEntityGenIntelliJTool) iterator.next();
            System.out.println(appInfoEntity.toString());
        }
    }

}