package enoxs.com.data.app;

import enoxs.com.util.HibernateUtil;
import org.hibernate.Session;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.*;

public class AppInfoTest {
    protected static Session session;

    @BeforeClass
    public static void beforeClass() {
        HibernateUtil.initConfig("enoxs.com.data.app",AppInfo.class);
        HibernateUtil.start();
        session = HibernateUtil.getSession();
    }

    @AfterClass
    public static void afterClass() {
        HibernateUtil.done();
    }

    @Test
    public void query() {
        List appInfos = session.createQuery("FROM enoxs.com.data.app.AppInfo ").list();
        for (Iterator iterator = appInfos.iterator(); iterator.hasNext(); ) {
            AppInfo appInfo = (AppInfo) iterator.next();
            System.out.println(appInfo.toString());
        }
    }

}