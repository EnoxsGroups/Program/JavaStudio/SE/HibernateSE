package enoxs.com.data.app;

import enoxs.com.data.gen.AppInfoGenEclipseJPAProject;
import enoxs.com.util.HibernateUtil;
import org.hibernate.Session;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Iterator;
import java.util.List;

public class AppInfoJPATest {
    private static Session session;
    @BeforeClass
    public static void beforeClass(){
        HibernateUtil.initConfig("enoxs.com.data.app",AppInfoGenEclipseJPAProject.class);
        HibernateUtil.start();
        session = HibernateUtil.getSession();
    }

    @AfterClass
    public static void afterClass(){
        HibernateUtil.done();
    }

    @Test
    public void query(){
        List appInfos = session.createQuery("FROM enoxs.com.data.app.AppInfoJPA ").list();
        for (Iterator iterator = appInfos.iterator(); iterator.hasNext(); ) {
            AppInfoGenEclipseJPAProject appInfoJPA = (AppInfoGenEclipseJPAProject) iterator.next();
            System.out.println(appInfoJPA.toString());
        }
    }
}