Hibernate Tutorial
======

table of contents
------
1. Hibernate - Home
2. ORM - Overview
3. Overview(概述)
4. Architecture(架構)
5. Environment(環境)
6. Configuration(設定)
7. Sessions(會話)
8. Persistent Class(持久類)
9. Mapping Files(映射文件)
10. Mapping Types(映射類型)
11. Examples(範例)
12. O/R Mappings(O/R映射)
13. Annotations(註解)
14. Query Language(查詢語言)
15. Criteria Queries(標準查詢)
16. Native SQL(原生 SQL)
17. Caching(快取)
18. Batch Processing(批次處理)
19. Interceptors(攔截器)

Reference
------
Hibernate Tutorial
<https://www.tutorialspoint.com/hibernate/index.htm>