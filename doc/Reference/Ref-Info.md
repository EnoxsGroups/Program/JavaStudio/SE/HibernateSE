Reference
======


Download
------
### Hibernate - Main
<http://hibernate.org/orm/releases/>

### Hibernate - Annotation
<https://sourceforge.net/projects/hibernate/files/hibernate-annotations/>


Website
------

Hibernate Tutorial
<https://www.tutorialspoint.com/hibernate/index.htm>

Hibernate下載與環境搭建教程
<https://www.itread01.com/content/1547940251.html>



Issue
------

JPA 報錯 SQLGrammarException: error performing isolated work
<https://www.itread01.com/content/1543086369.html>

使用JDBC连接MySql时出现：The server time zone value '�й���׼ʱ��' is unrecognized
<https://www.cnblogs.com/EasonJim/p/6906713.html>