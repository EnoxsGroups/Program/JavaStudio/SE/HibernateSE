ISSUE01
======

JPA 報錯 SQLGrammarException: error performing isolated work
------
原始：

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    
修改：
    
    @Id @GeneratedValue
    @Column(name = "id")
    private int id;
