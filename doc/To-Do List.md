Hibernate To-Do List
======

+ [X] 快速閱覽 - 中/英 對照
+ [X] 確定重點閱讀與實作範圍 - 列表

閱讀
------

實作
------
+ [X] impl : Environment
+ [X] impl : Example
+ [X] init : SQL Database from mybatis project;

+ [ ] impl : O/R Mappings
    + Collections Mappings
        + [X] Set
        + [X] SortedSet
        + [X] List
        + [ ] Collection
        + [X] Map
        + [ ] SortedMap
    + Association Mappings
        + [ ] Many-to-One
        + [ ] One-to-One
        + [ ] One-to-Many
        + [ ] Many-to-Many
    + Component Mappings
        + [ ] Component Mappings


+ [X] impl : Annotations
+ [X] impl : Hibernate - Query Language 

+ [X] impl : Criteria Queries
+ [X] impl : Native SQL
+ [X] impl : Batch Processing


+ [ ] Review - Criteria Queries , AppInfo , Example with @Annotation.

+ [ ] Mayaminer - JPA , Generate
+ [ ] Review Annotation with JPA - Main method of use

+ [ ] Impl - Caching
+ [ ] Impl - Interceptors

+ [ ] Remove - Custome Demo Example



+ [X] impl : HibernateUtil
+ [ ] annotaion - HibernateUtil 


研究
------

+ [ ] why Criteria Queries(標準查詢) &&  Native SQL(原生 SQL) was deprecated.
+ [ ] what is javax.persistence.* ?