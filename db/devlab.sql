-- fast demo
-- com.bean

-- Customer
CREATE TABLE `cst_customer` (
  `cust_id` bigint(32) NOT NULL AUTO_INCREMENT COMMENT '客戶編號(主鍵)',
  `cust_name` varchar(32) NOT NULL COMMENT '客戶名稱(公司名稱)',
  `cust_user_id` bigint(32) DEFAULT NULL COMMENT '負責人id',
  `cust_create_id` bigint(32) DEFAULT NULL COMMENT '建立人id',
  `cust_source` varchar(32) DEFAULT NULL COMMENT '客戶資訊來源',
  `cust_industry` varchar(32) DEFAULT NULL COMMENT '客戶所屬行業',
  `cust_level` varchar(32) DEFAULT NULL COMMENT '客戶級別',
  `cust_linkman` varchar(64) DEFAULT NULL COMMENT '聯絡人',
  `cust_phone` varchar(64) DEFAULT NULL COMMENT '固定電話',
  `cust_mobile` varchar(16) DEFAULT NULL COMMENT '行動電話',
  PRIMARY KEY (`cust_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
