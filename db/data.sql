-- app_info
insert into app_info(name,version,author,date,remark) values
('JavaProjSE-v1.0.3','1.0.3','Enoxs','2019-07-24' ,'Java Project Simple Example - Version 1.0.3'),
('JUnitSE','1.0.2','Enoxs','2019-08-17' ,'Java Unit Test Simple Example'),
('SpringMVC-SE','1.0.2','Enoxs','2019-07-31' ,'Java Web Application Spring MVC 框架 - Simple Example 未整合持久化框架（MyBatis）');

-- user_info
insert into user_info(name,account,password,remark) values
('夢想架構師','Enoxs','00000000','專注於 Java / Android，架構夢想的軟體開發工程師'),
('系統管理人員','admin','root0000','最高管理權限，系統人員'),
('測試人員','Test','0000','一般操作權限，使用者');

-- app_group
insert into app_group(name,description) values
('Program','程式範例'),
('JavaStudio','Java 程式範例'),
('SE','Java 程式範例 SE 系列');

-- app_sub_group
insert into app_sub_group(group_id,sub_group_id) values
(1,2),
(2,3);

-- app_group_project
insert into app_group_project(group_id,app_id) values
(3,1),
(3,2),
(3,3);

