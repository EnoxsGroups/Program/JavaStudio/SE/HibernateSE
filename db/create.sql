-- AppMain
-- enoxs.com.hibernate

-- app_info
drop table if exists app_info;
create table app_info (
	id int auto_increment primary key ,
	name	nvarchar(50),
	version	nvarchar(30),
	author	nvarchar(50),
	date	datetime,
	remark	nvarchar(100)
);

-- user_info

drop table if exists user_info;
create table user_info (
	id int auto_increment primary key ,
	name nvarchar(50),
	account nvarchar(50),
	password nvarchar(50),
	remark nvarchar(100)
);

-- app_group

drop table if exists app_group;
create table app_group (
	id int auto_increment primary key ,
	name nvarchar(50),
	description nvarchar(100)
);

-- app_sub_group

drop table if exists app_sub_group;
create table app_sub_group (
	group_id integer,
	sub_group_id integer
);

-- app_group_project

drop table if exists app_group_project;
create table app_group_project (
	group_id integer,
	app_id integer
);

