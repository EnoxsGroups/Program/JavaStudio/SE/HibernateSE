package enoxs.com.hibernate;

import enoxs.com.data.app.AppInfo;
import enoxs.com.util.HibernateUtil;
import org.hibernate.Session;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class S1_Example {
    protected static Session session;

    @BeforeClass
    public static void beforeClass() {
        HibernateUtil.start();
        session = HibernateUtil.getSession();
    }

    @AfterClass
    public static void afterClass() {
        HibernateUtil.done();
    }

    @Test
    public void query() {
        List appInfos = session.createQuery("FROM AppInfo ").list();
        for (Iterator iterator = appInfos.iterator(); iterator.hasNext(); ) {
            AppInfo appInfo = (AppInfo) iterator.next();
            printAppInfoMsg(appInfo);
        }
    }

    @Test
    public void add(){
        AppInfo appInfo = createAppInfoData();
        Integer id = (Integer) session.save(appInfo);
        System.out.println("id -> " + id);
    }

    @Test
    public void queryById(){
        Integer id = 1;
        AppInfo appInfo = (AppInfo)session.get(AppInfo.class, id);
        printAppInfoMsg(appInfo);
    }

    @Test
    public void modify(){
        Integer id = 1;
        AppInfo appInfo = (AppInfo)session.get(AppInfo.class,id);
        printAppInfoMsg(appInfo);

        appInfo.setVersion("1.0.5");
        appInfo.setRemark("Java Project Simple Example - Version 1.0.5");
        session.update(appInfo);
        printAppInfoMsg(appInfo);
    }

    @Test
    public void remove(){
        Integer id = 5;
        AppInfo appInfo = (AppInfo)session.get(AppInfo.class,id);
        printAppInfoMsg(appInfo);

        session.delete(appInfo);
        query();
    }

    private AppInfo createAppInfoData(){
        AppInfo appInfo = new AppInfo();
        appInfo.setName("HibernateSE");
        appInfo.setVersion("v1.0.1");
        appInfo.setAuthor("Enoxs");
        appInfo.setDate(new Date());
        appInfo.setRemark("test add().");
        return appInfo;
    }

    private void printAppInfoMsg(AppInfo appInfo){
        if(appInfo != null){
            StringBuffer sb = new StringBuffer(512);
            sb.append(appInfo.getId());
            sb.append(" , ");
            sb.append(appInfo.getName());
            sb.append(" , ");
            sb.append(appInfo.getVersion());
            sb.append(" , ");
            sb.append(appInfo.getAuthor());
            sb.append(" , ");
            sb.append(appInfo.getDate());
            sb.append(" , ");
            sb.append(appInfo.getRemark());
            System.out.println(sb.toString());
        }
    }
}
