package enoxs.com.util;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
    private static SessionFactory factory;
    private static Configuration config;
    private static Session session;
    private static Transaction transaction;

    public static void initConfig(){
        config = new Configuration().configure();
    }

    public static void initConfig(String packageName, Class annotatedClass){
        config = new Configuration().configure()
                .addPackage(packageName)
                .addAnnotatedClass(annotatedClass);
    }

    public static void start(){
        try {

            if(config == null){
                initConfig();
            }
            factory = config.buildSessionFactory();
            session = factory.openSession();
            transaction = session.beginTransaction();
        } catch (Throwable ex) {
            System.err.println("Failed to create sessionFactory object." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static Session getSession(){
        return session;
    }

    public static void done(){
        try {
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction!=null) transaction.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }
}
