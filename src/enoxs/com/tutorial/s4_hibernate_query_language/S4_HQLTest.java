package enoxs.com.tutorial.s4_hibernate_query_language;

import enoxs.com.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

public class S4_HQLTest {
    protected static Session session;

    @BeforeClass
    public static void beforeClass() {
        HibernateUtil.start();
        session = HibernateUtil.getSession();
    }

    @AfterClass
    public static void afterClass() {
        HibernateUtil.done();
    }

    @Test
    public void queryFromClause01(){
        String hql = "FROM AppInfo";
        Query query = session.createQuery(hql);
        List results = query.list();

        for (Object result : results) {
            System.out.println(result.toString());
        }
    }

    @Test
    public void queryFromClause02(){
        String hql = "FROM enoxs.com.data.app.AppInfo";
        Query query = session.createQuery(hql);
        List results = query.list();

        for (Object result : results) {
            System.out.println(result.toString());
        }
    }

    @Test
    public void asClause01(){
        String hql = "FROM AppInfo AS appInfo";
        Query query = session.createQuery(hql);
        List results = query.list();

        for (Object result : results) {
            System.out.println(result.toString());
        }
    }

    @Test
    public void asClause02(){
        String hql = "FROM AppInfo appInfo";
        Query query = session.createQuery(hql);
        List results = query.list();

        for (Object result : results) {
            System.out.println(result.toString());
        }
    }


    /**
     * one parameter -> table type
     * many parameter -> Object []
     */

    // Enoxs To-Do : Study - Query Many Parameter From Table
    @Test
    public void selectClause(){
        String hql = "SELECT appInfo.name FROM AppInfo as appInfo"; // ,appInfo.version,appInfo.author
        Query query = session.createQuery(hql);
        List results = query.list();
        for (Object result : results) {
//            String className = result.getClass().getName();
//            System.out.println(className);
            System.out.println(result.toString());
        }
    }

    @Test
    public void  whereClause(){
        String hql = "FROM AppInfo appInfo WHERE appInfo.name = 'JavaProjSE-v1.0.3'";
        Query query = session.createQuery(hql);
        List results = query.list();

        for (Object result : results) {
            System.out.println(result.toString());
        }
    }

    @Test
    public void orderByClause01(){
        String hql = "FROM AppInfo appInfo WHERE appInfo.author = 'Enoxs' ORDER BY appInfo.date DESC";
        Query query = session.createQuery(hql);
        List results = query.list();

        for (Object result : results) {
            System.out.println(result.toString());
        }
    }

    @Test
    public void orderByClause02(){
        String hql = "FROM AppInfo appInfo WHERE appInfo.author = 'Enoxs' " +
                "ORDER BY appInfo.id ASC , appInfo.date DESC ";
        Query query = session.createQuery(hql);
        List results = query.list();

        for (Object result : results) {
            System.out.println(result.toString());
        }
    }

    @Test
    public void groupByClause(){
        String hql = "SELECT SUM(appInfo.id), appInfo.name FROM AppInfo appInfo " +
                "GROUP BY appInfo.name";
        Query query = session.createQuery(hql);
        List<Object []> results = query.list();

        for (Object [] result : results) {
            System.out.println(result[0].toString());
            System.out.println(result[1].toString());
        }
    }

    @Test
    public void usingNamedParameters(){
        String hql = "FROM AppInfo appInfo WHERE appInfo.name = :appName";
        Query query = session.createQuery(hql);
        query.setParameter("appName","SpringMVC-SE");
        List results = query.list();

        for (Object result : results) {
            System.out.println(results.toString());
        }
    }

    @Test
    public void updateClause(){
        String hql = "UPDATE AppInfo set author = :appAuthor "  +
                "WHERE name = :appName";
        Query query = session.createQuery(hql);
        query.setParameter("appAuthor", "Spring");
        query.setParameter("appName", "SpringMVC-SE");
        int result = query.executeUpdate();
        System.out.println("Rows affected: " + result);
    }

    @Test
    public void deleteClause(){
        String hql = "DELETE FROM AppInfo "  +
                "WHERE name = :appName";
        Query query = session.createQuery(hql);
        query.setParameter("appName", "HibernateSE");
        int result = query.executeUpdate();
        System.out.println("Rows affected: " + result);
    }

    @Test
    public void insertClause(){
        String hql = "INSERT INTO AppInfo (name, version, author,date,remark)"  +
                "SELECT name, version, 'system', current_timestamp , 'init: new record.' FROM AppInfo WHERE name = :appName";
        Query query = session.createQuery(hql);
        query.setParameter("appName", "JavaProjSE-v1.0.3");
        int result = query.executeUpdate();
        System.out.println("Rows affected: " + result);
    }

//    // Special Method

    @Test
    public void aggregateMethods(){
        String hql = "SELECT count(distinct appInfo.author) FROM AppInfo appInfo";
        Query query = session.createQuery(hql);
        List results = query.list();

        for (Object result : results) {
            System.out.println(result);
        }

    }

    @Test
    public void paginationUsingQuery(){
        String hql = "FROM AppInfo ";
        Query query = session.createQuery(hql);
        query.setFirstResult(1);
        query.setMaxResults(2);
        List results = query.list();

        for (Object result : results) {
            System.out.println(result.toString());
        }
    }
}
