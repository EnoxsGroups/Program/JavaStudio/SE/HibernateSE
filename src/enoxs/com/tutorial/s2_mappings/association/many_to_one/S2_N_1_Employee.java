package enoxs.com.tutorial.s2_mappings.association.many_to_one;

public class S2_N_1_Employee {
    private int id;
    private String firstName;
    private String lastName;
    private int salary;
    private S2_N_1_Address address;

    public S2_N_1_Employee() {}

    public S2_N_1_Employee(String fname, String lname, int salary, S2_N_1_Address address ) {
        this.firstName = fname;
        this.lastName = lname;
        this.salary = salary;
        this.address = address;
    }

    public int getId() {
        return id;
    }

    public void setId( int id ) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName( String first_name ) {
        this.firstName = first_name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName( String last_name ) {
        this.lastName = last_name;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary( int salary ) {
        this.salary = salary;
    }

    public S2_N_1_Address getAddress() {
        return address;
    }

    public void setAddress( S2_N_1_Address address ) {
        this.address = address;
    }
}
