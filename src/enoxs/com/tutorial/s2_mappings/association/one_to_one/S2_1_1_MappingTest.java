package enoxs.com.tutorial.s2_mappings.association.one_to_one;

import org.junit.BeforeClass;
import org.junit.Test;
import java.util.*;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class S2_1_1_MappingTest {
    private static SessionFactory factory;

    @BeforeClass
    public static void beforeClass(){
        try {
            factory = new Configuration().configure().buildSessionFactory();
        } catch (Throwable ex) {
            System.err.println("Failed to create sessionFactory object." + ex);
            throw new ExceptionInInitializerError(ex);
        }

    }

    @Test
    public void listEmployeesTest(){
        listEmployees();
    }

    @Test
    public void S2_1_1_MappingTest() {
        S2_1_1_MappingTest ME = new S2_1_1_MappingTest();

        /* Let us have one address object */
        S2_1_1_Address address1 = ME.addAddress("Kondapur","Hyderabad","AP","532");

        /* Add employee records in the database */
        Integer empID1 = ME.addEmployee("Manoj", "Kumar", 4000, address1);

        /* Let us have another address object */
        S2_1_1_Address address2 = ME.addAddress("Saharanpur","Ambehta","UP","111");

        /* Add another employee record in the database */
        Integer empID2 = ME.addEmployee("Dilip", "Kumar", 3000, address2);

        /* List down all the employees */
        ME.listEmployees();

        /* Update employee's salary records */
        ME.updateEmployee(empID1, 5000);

        /* List down all the employees */
        ME.listEmployees();

    }

    /* Method to add an address record in the database */
    public S2_1_1_Address addAddress(String street, String city, String state, String zipcode) {
        Session session = factory.openSession();
        Transaction tx = null;
        Integer addressID = null;
        S2_1_1_Address address = null;

        try {
            tx = session.beginTransaction();
            address = new S2_1_1_Address(street, city, state, zipcode);
            addressID = (Integer) session.save(address);
            tx.commit();
        } catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return address;
    }

    /* Method to add an employee record in the database */
    public Integer addEmployee(String fname, String lname, int salary, S2_1_1_Address address){
        Session session = factory.openSession();
        Transaction tx = null;
        Integer employeeID = null;

        try {
            tx = session.beginTransaction();
            S2_1_1_Employee employee = new S2_1_1_Employee(fname, lname, salary, address);
            employeeID = (Integer) session.save(employee);
            tx.commit();
        } catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return employeeID;
    }

    /* Method to list all the employees detail */
    public void listEmployees( ){
        Session session = factory.openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            List employees = session.createQuery("FROM S2_1_1_Employee").list();
            for (Iterator iterator = employees.iterator(); iterator.hasNext();){
                S2_1_1_Employee employee = (S2_1_1_Employee) iterator.next();
                System.out.print("First Name: " + employee.getFirstName());
                System.out.print("  Last Name: " + employee.getLastName());
                System.out.println("  Salary: " + employee.getSalary());
                S2_1_1_Address add = employee.getAddress();
                System.out.println("Address ");
                System.out.println("\tStreet: " +  add.getStreet());
                System.out.println("\tCity: " + add.getCity());
                System.out.println("\tState: " + add.getState());
                System.out.println("\tZipcode: " + add.getZipcode());
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    /* Method to update salary for an employee */
    public void updateEmployee(Integer EmployeeID, int salary ){
        Session session = factory.openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            S2_1_1_Employee employee = (S2_1_1_Employee)session.get(S2_1_1_Employee.class, EmployeeID);
            employee.setSalary( salary );
            session.update(employee);
            tx.commit();
        } catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }
}
