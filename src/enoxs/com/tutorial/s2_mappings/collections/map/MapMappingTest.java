package enoxs.com.tutorial.s2_mappings.collections.map;

import org.junit.BeforeClass;
import org.junit.Test;

import java.util.*;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class MapMappingTest {

    private static SessionFactory factory;

    @BeforeClass
    public static void beforeClass(){
        try{
            factory = new Configuration().configure().buildSessionFactory();
        }catch (Throwable ex) {
            System.err.println("Failed to create sessionFactory object." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }


    @Test
    public void listEmployeesTest(){
        listEmployees();
    }

    /**
     * Main Test
     */
    @Test
    public void mapMappingTest(){
        MapMappingTest ME = new MapMappingTest();
        /* Let us have a set of certificates for the first employee  */
        HashMap set = new HashMap();
        set.put("ComputerScience", new MapCertificate("MCA"));
        set.put("BusinessManagement", new MapCertificate("MBA"));
        set.put("ProjectManagement", new MapCertificate("PMP"));

        /* Add employee records in the database */
        Integer empID = ME.addEmployee("Manoj", "Kumar", 4000, set);

        /* List down all the employees */
        ME.listEmployees();

        /* Update employee's salary records */
        ME.updateEmployee(empID, 5000);

        /* List down all the employees */
        ME.listEmployees();

    }

    /* Method to add an employee record in the database */
    public Integer addEmployee(String fname, String lname, int salary, HashMap cert){
        Session session = factory.openSession();
        Transaction tx = null;
        Integer employeeID = null;
        try{
            tx = session.beginTransaction();
            MapEmployee employee = new MapEmployee(fname, lname, salary);
            employee.setCertificates(cert);
            employeeID = (Integer) session.save(employee);
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
        return employeeID;
    }

    /* Method to list all the employees detail */
    public void listEmployees( ){
        Session session = factory.openSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            List employees = session.createQuery("FROM MapEmployee").list();
            for (Iterator iterator1 = employees.iterator(); iterator1.hasNext();){
                MapEmployee employee = (MapEmployee) iterator1.next();
                System.out.print("First Name: " + employee.getFirstName());
                System.out.print("  Last Name: " + employee.getLastName());
                System.out.println("  Salary: " + employee.getSalary());
                Map ec = employee.getCertificates();
                System.out.println("Certificate: " +
                        (((MapCertificate)ec.get("ComputerScience")).getName()));
                System.out.println("Certificate: " +
                        (((MapCertificate)ec.get("BusinessManagement")).getName()));
                System.out.println("Certificate: " +
                        (((MapCertificate)ec.get("ProjectManagement")).getName()));
            }
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
    }
    /* Method to update salary for an employee */
    public void updateEmployee(Integer EmployeeID, int salary ){
        Session session = factory.openSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            MapEmployee employee = (MapEmployee)session.get(MapEmployee.class, EmployeeID);
            employee.setSalary( salary );
            session.update(employee);
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
    }
    /* Method to delete an employee from the records */
    public void deleteEmployee(Integer EmployeeID){
        Session session = factory.openSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            MapEmployee employee = (MapEmployee)session.get(MapEmployee.class, EmployeeID);
            session.delete(employee);
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
    }
}
