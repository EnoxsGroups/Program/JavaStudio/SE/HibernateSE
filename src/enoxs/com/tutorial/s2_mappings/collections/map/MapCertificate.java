package enoxs.com.tutorial.s2_mappings.collections.map;

public class MapCertificate {
    private int id;
    private String name;

    public MapCertificate() {}

    public MapCertificate(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId( int id ) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName( String name ) {
        this.name = name;
    }
}
