package enoxs.com.tutorial.s2_mappings.collections.list;

public class ListCertificate {
    private int id;
    private String name;

    public ListCertificate() {}

    public ListCertificate(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId( int id ) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName( String name ) {
        this.name = name;
    }
}
