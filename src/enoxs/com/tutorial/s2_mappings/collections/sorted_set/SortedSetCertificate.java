package enoxs.com.tutorial.s2_mappings.collections.sorted_set;

public class SortedSetCertificate implements Comparable <SortedSetCertificate>{
    private int id;
    private String name;

    public SortedSetCertificate() {}

    public SortedSetCertificate(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId( int id ) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName( String name ) {
        this.name = name;
    }

    public int compareTo(SortedSetCertificate that){
        final int BEFORE = -1;
        final int AFTER = 1;

        if (that == null) {
            return BEFORE;
        }

        Comparable thisCertificate = this.getName();
        Comparable thatCertificate = that.getName();

        if(thisCertificate == null) {
            return AFTER;
        } else if(thatCertificate == null) {
            return BEFORE;
        } else {
            return thisCertificate.compareTo(thatCertificate);
        }
    }
}
