package enoxs.com.tutorial.s2_mappings.collections.sorted_set;

import java.util.Comparator;

public class SortedSetMyClass implements Comparator<SortedSetCertificate> {
    public int compare(SortedSetCertificate o1, SortedSetCertificate o2) {
        final int BEFORE = -1;
        final int AFTER = 1;

        /* To reverse the sorting order, multiple by -1 */
        if (o2 == null) {
            return BEFORE * -1;
        }

        Comparable thisCertificate = o1.getName();
        Comparable thatCertificate = o2.getName();

        if(thisCertificate == null) {
            return AFTER * 1;
        } else if(thatCertificate == null) {
            return BEFORE * -1;
        } else {
            return thisCertificate.compareTo(thatCertificate) * -1;
        }
    }
}
