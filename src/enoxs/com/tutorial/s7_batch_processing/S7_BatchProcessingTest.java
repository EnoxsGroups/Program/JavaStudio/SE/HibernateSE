package enoxs.com.tutorial.s7_batch_processing;

import org.junit.BeforeClass;
import org.junit.Test;
import java.util.*;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class S7_BatchProcessingTest {
    private static SessionFactory factory;

    @BeforeClass
    public static void beforeClass(){
        try {
            factory = new Configuration().configure().buildSessionFactory();
        } catch (Throwable ex) {
            System.err.println("Failed to create sessionFactory object." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }


    @Test
    public void batchProcessingTest(){
        S7_BatchProcessingTest ME = new S7_BatchProcessingTest();

        /* Add employee records in batches */
        ME.addEmployees( );
    }

    /* Method to create employee records in batches */
    public void addEmployees( ){
        Session session = factory.openSession();
        Transaction tx = null;
        Integer employeeID = null;

        try {
            tx = session.beginTransaction();
            for ( int i=0; i<500; i++ ) {
                String fname = "First Name " + i;
                String lname = "Last Name " + i;
                Integer salary = i;
                S7_Employee employee = new S7_Employee(fname, lname, salary);
                session.save(employee);
                if( i % 50 == 0 ) {
                    session.flush();
                    session.clear();
                }
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return ;
    }


}
