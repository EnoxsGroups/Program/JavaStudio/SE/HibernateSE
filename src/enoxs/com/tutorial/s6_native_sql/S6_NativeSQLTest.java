package enoxs.com.tutorial.s6_native_sql;

import org.junit.BeforeClass;
import org.junit.Test;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.SessionFactory;
import org.hibernate.SQLQuery;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.cfg.Configuration;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class S6_NativeSQLTest {
    private static SessionFactory factory;


    @BeforeClass
    public static void beforeClass(){
        try {
            factory = new Configuration().configure().buildSessionFactory();
        } catch (Throwable ex) {
            System.err.println("Failed to create sessionFactory object." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    /**
     * Scalar Queries
     */
    @Test
    public void listEmployeesScalarTest(){
        listEmployeesScalar();
    }

    /**
     * Entity Queries
     */
    @Test
    public void listEmployeesEntityTest(){
        listEmployeesEntity();
    }

    /**
     * Named SQL Queries
     */
    @Test
    public void nameSqlQueryTest(){
        nameSqlQuery();
    }


    /**
     * Main Test
     */
    @Test
    public void nativeSQLTest(){

        S6_NativeSQLTest ME = new S6_NativeSQLTest();

        /* Add few employee records in database */
        Integer empID1 = ME.addEmployee("Zara", "Ali", 2000);
        Integer empID2 = ME.addEmployee("Daisy", "Das", 5000);
        Integer empID3 = ME.addEmployee("John", "Paul", 5000);
        Integer empID4 = ME.addEmployee("Mohd", "Yasee", 3000);

        /* List down employees and their salary using Scalar Query */
        ME.listEmployeesScalar();

        /* List down complete employees information using Entity Query */
        ME.listEmployeesEntity();
    }

    /* Method to CREATE an employee in the database */
    public Integer addEmployee(String fname, String lname, int salary){
        Session session = factory.openSession();
        Transaction tx = null;
        Integer employeeID = null;

        try {
            tx = session.beginTransaction();
            S6_Employee employee = new S6_Employee(fname, lname, salary);
            employeeID = (Integer) session.save(employee);
            tx.commit();
        } catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return employeeID;
    }

    /* Method to  READ all the employees using Scalar Query */
    public void listEmployeesScalar( ){
        Session session = factory.openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            String sql = "SELECT first_name, salary FROM EMPLOYEE";
            SQLQuery query = session.createSQLQuery(sql);
            query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
            List data = query.list();

            for(Object object : data) {
                Map row = (Map)object;
                System.out.print("First Name: " + row.get("first_name"));
                System.out.println(", Salary: " + row.get("salary"));
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    /* Method to READ all the employees using Entity Query */
    public void listEmployeesEntity( ){
        Session session = factory.openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            String sql = "SELECT * FROM EMPLOYEE";
            SQLQuery query = session.createSQLQuery(sql);
            query.addEntity(S6_Employee.class);
            List employees = query.list();

            for (Iterator iterator = employees.iterator(); iterator.hasNext();){
                S6_Employee employee = (S6_Employee) iterator.next();
                System.out.print("First Name: " + employee.getFirstName());
                System.out.print("  Last Name: " + employee.getLastName());
                System.out.println("  Salary: " + employee.getSalary());
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public void nameSqlQuery(){
        Session session = factory.openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            String sql = "SELECT * FROM EMPLOYEE WHERE id = :employee_id";
            SQLQuery query = session.createSQLQuery(sql);
            query.addEntity(S6_Employee.class);
            query.setParameter("employee_id", 12);
            List results = query.list();

            for (Iterator iterator = results.iterator(); iterator.hasNext();){
                S6_Employee employee = (S6_Employee) iterator.next();
                System.out.print("First Name: " + employee.getFirstName());
                System.out.print("  Last Name: " + employee.getLastName());
                System.out.println("  Salary: " + employee.getSalary());
            }

            tx.commit();
        } catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }
}
