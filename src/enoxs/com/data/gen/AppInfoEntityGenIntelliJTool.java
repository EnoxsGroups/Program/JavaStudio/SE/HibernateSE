package enoxs.com.data.gen;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * Hibernate Generator Tools
 * Menu -> View -> Tool Windows -> Persistence -> Right Click ->
 * Generate Persistence Mapping -> By Database Schema
 */
@Entity
@Table(name = "app_info", schema = "hibernate", catalog = "")
public class AppInfoEntityGenIntelliJTool {
    private int id;
    private String name;
    private String version;
    private String author;
    private Timestamp date;
    private String remark;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "version")
    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @Basic
    @Column(name = "author")
    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Basic
    @Column(name = "date")
    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    @Basic
    @Column(name = "remark")
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AppInfoEntityGenIntelliJTool that = (AppInfoEntityGenIntelliJTool) o;
        return id == that.id &&
                Objects.equals(name, that.name) &&
                Objects.equals(version, that.version) &&
                Objects.equals(author, that.author) &&
                Objects.equals(date, that.date) &&
                Objects.equals(remark, that.remark);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name, version, author, date, remark);
    }

    @Override
    public String toString() {
        return "AppInfoEntityGenIntelliJTool{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", version='" + version + '\'' +
                ", author='" + author + '\'' +
                ", date=" + date +
                ", remark='" + remark + '\'' +
                '}';
    }
}
