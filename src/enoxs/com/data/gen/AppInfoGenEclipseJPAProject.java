package enoxs.com.data.gen;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;

/**
 * JPA Generator Tools
 * 1. New JAP Project
 * 2. Window -> Show View -> Data Source Explorer
 * 3. New Connection Connect Database
 * 4. JAP Project ->
 * 
 * ref: <https://www.eclipse.org/webtools/dali/docs/3.2/user_guide/tasks006.htm>
 * 
 */

@Entity
@Table(name="app_info")
@NamedQuery(name="AppInfo.findAll", query="SELECT a FROM enoxs.com.data.app.AppInfoJPA a")
public class AppInfoGenEclipseJPAProject implements Serializable{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy=GenerationType.TABLE)
    @Column(unique=true, nullable=false)
    private int id;
    private String name;
    private String version;
    private String author;
    private Timestamp date;
    private String remark;

    public AppInfoGenEclipseJPAProject() {
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAuthor() {
        return this.author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Timestamp getDate() {
        return this.date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public Object getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getRemark() {
        return this.remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Object getVersion() {
        return this.version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
