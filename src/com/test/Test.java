package com.test;

import com.bean.Customer;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class Test {
    public static void main(String[] args) {
        // 儲存一個數據到資料庫
        Configuration conf = new Configuration().configure();
        SessionFactory sessionFactory = conf.buildSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        // -------------------------------------------
        Customer customer = new Customer();
        customer.setCust_name("EnoxsOwO");
        session.save(customer);
        // -------------------------------------------
        transaction.commit();
        session.close();
        sessionFactory.close();
    }
}
